import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        int res = a + b;
        return res;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        int min = liste.get(0);
        for(int i = 0 ; i < liste.size(); i++){
            if(min <= liste.get(i)){
                min = liste.get(i);
            }
        }
        return min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for(int i = 0 ; i < liste.size(); i++){
            if(valeur.compareTo(liste.get(i)) >= 0){
                return false;
            }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        ArrayList<T> res = new ArrayList<>();
        while(liste1.size()>0 || liste2.size()>0){
            if(liste1.size()==0){
                res.add(liste2.get(0));
                liste1.remove(liste2.get(0));
            }
            else if(liste2.size()==0){
                res.add(liste1.get(0));
                liste1.remove(liste1.get(0));
            }
            else if(liste1.get(0).compareTo(liste2.get(0)) < 0){
                res.add(liste1.get(0));
                liste1.remove(liste1.get(0));
            }
            else if(liste1.get(0).compareTo(liste2.get(0)) == 0){
                res.add(liste1.get(0));
                res.add(liste2.get(0));
                liste1.remove(liste1.get(0));
                liste1.remove(liste2.get(0));
            }
            else{
                res.add(liste2.get(0));
                liste1.remove(liste2.get(0));
            }
        }
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        ArrayList<String> res = new ArrayList<>();
        for(String elem : texte.split(" ")){
            res.add(elem);
        }
        return res
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        ArrayList<String> liste = decoupe(String texte);
        String court = liste.get(0);
        for(int i = 0 ; i < liste.size(); i++){
            if(court.compareTo(liste.get(i) >=0)){
                court=liste.get(i);
            }
        }
        return court;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        String ouvert = "(";
        String fermer = ")";
        int FoisOuvert = 0;
        int FoisFermer = 0;
        for (int i = 0 ; i < maChaine.length() ;i++){
            String lettre = maChaine.charAt(i);
            if(lettre.compareTo(ouvert) == 0){
                int FoisOuvert += 1;
            }
            else if(lettre.compareTo(fermer) == 0){
                int FoisFermer += 1;
            }
        }
        if(FoisFermer>FoisOuvert || FoisFermer < FoisOuvert){
            return false;
        }
        else{
            return true;
        }
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        return true;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        for(int i = 0 ; i < liste.size(); i++){
            if(liste.get(i) == valeur){
                return true;
            }
        }
        return false;
    }



}
